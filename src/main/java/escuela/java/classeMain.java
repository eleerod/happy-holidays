package escuela.java;

import escuela.java.Utilities.DateUtilities;
import escuela.java.Utilities.EntityManagerUtilities;
import escuela.java.model.Accommodation;
import escuela.java.model.City;
import escuela.java.model.Leisure;
import escuela.java.model.Restaurant;
import escuela.java.persistence.AccommodationPersistence;
import escuela.java.persistence.CityPersistence;
import escuela.java.persistence.LeisurePersistence;
import escuela.java.persistence.RestaurantPersistence;

import javax.json.Json;
import javax.json.JsonObjectBuilder;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

public class classeMain {
    public static void main(String[] args) {
        EntityManager entityManager = EntityManagerUtilities.getEntityManager();
        //EntityTransaction transaction = entityManager.getTransaction();
        /*transaction.begin();
        List<Accommodation> accommodation = entityManager.createQuery("Select a from Accommodation a").getResultList();
        transaction.commit();*/
        //System.out.println(accommodation);

        /*CityPersistence cityPersistence = new CityPersistence(entityManager);
        City city = cityPersistence.getCity("Barcelona");

        Date date = DateUtilities.getDateFromString("2018-07-02");
        Date date2 = DateUtilities.getDateFromString("2018-07-03");
        LeisurePersistence leisurePersistence = new LeisurePersistence(entityManager);
        System.out.println(date);
        List<Leisure> list = leisurePersistence.getLeisureRecommendations(date, date2, city);
        System.out.println(list);

        AccommodationPersistence accommodationPersistence = new AccommodationPersistence(entityManager);
        List<Accommodation> accommodations = accommodationPersistence.getAccomodationsRecomendations(date,date2,city);
        System.out.println(accommodations);

        RestaurantPersistence restaurantPersistence = new RestaurantPersistence(entityManager);
        List<Restaurant> restaurants = restaurantPersistence.getRestaurantRecommendations(city);
        System.out.println(restaurants);*/

        /*List<Leisure> res = leisurePersistence.getLeisure();
        System.out.println(res);*/

        CityPersistence cityPersistence = new CityPersistence(entityManager);
        //System.out.println(cityPersistence.existsCity("aaa"));
        //System.out.println(cityPersistence.getCity("aaa"));
        //cityPersistence.updateCity(8,new City("Amposta","ES"));
        //cityPersistence.addCity(new City("Hospitalet","ES"));

        /*AccommodationPersistence accommodationPersistence = new AccommodationPersistence(entityManager);
        Accommodation pepe = accommodationPersistence.getAccomodation(4);
        pepe.setAddress("address pepe");
        accommodationPersistence.updateAccommodation(4, pepe);*/
        /*try {
            DateUtilities.getDateFromString("2018-13-31");
        }catch(ParseException P){
            P.printStackTrace();
        }*/

        JsonObjectBuilder jsonObjectBuilder = Json.createObjectBuilder();
        JsonObjectBuilder jsonCityBuilder = Json.createObjectBuilder();
        jsonCityBuilder.add("country","ES");
        jsonCityBuilder.add("id",1);
        jsonCityBuilder.add("name","Barcelona");
        jsonObjectBuilder.add("name","Hostal Henrique");
        jsonObjectBuilder.add("address","Calle falsa");
        jsonObjectBuilder.add("rating",3);
        jsonObjectBuilder.add("start_date","2018-08-01T00:00:00Z[UTC]");
        jsonObjectBuilder.add("end_date","2018-08-01T00:00:00Z[UTC]");
        jsonObjectBuilder.add("city",jsonCityBuilder);
        System.out.println(jsonObjectBuilder.build());


    }
}
