package escuela.java.persistence;

import escuela.java.model.City;
import escuela.java.model.Leisure;
import javax.persistence.EntityManager;
import java.util.Date;
import java.util.List;

public class LeisurePersistence {

    EntityManager entityManager;

    private static final String ORDERED_LEISURES_BY_CITI_AND_DATE =
                                                "SELECT l from Leisure l " +
                                                "WHERE l.city LIKE :city " +
                                                "AND l.start_date <= :enddate " +
                                                "AND l.end_date >= :startdate " +
                                                "AND l.enabled = 1 " +
                                                "ORDER BY l.sponsored DESC, l.rating DESC";

    public LeisurePersistence(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public List<Leisure> getLeisureRecommendations(Date initDate, Date endDate, City city){
        return (List<Leisure>) entityManager.createQuery(ORDERED_LEISURES_BY_CITI_AND_DATE)
                .setParameter("city",city)
                .setParameter("startdate", initDate)
                .setParameter("enddate", endDate)
                .setMaxResults(5)
                .getResultList();
    }
}
