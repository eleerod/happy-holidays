package escuela.java.persistence;

import escuela.java.model.City;
import escuela.java.model.Restaurant;

import javax.persistence.EntityManager;
import java.util.List;

public class RestaurantPersistence {
    EntityManager entityManager;

    private static final String ORDERED_LEISURES_BY_CITI_AND_DATE =
                                                "SELECT r from Restaurant r " +
                                                "WHERE r.city LIKE :city " +
                                                "AND r.enabled = 1 " +
                                                "ORDER BY r.sponsored DESC, r.rating DESC";

    public RestaurantPersistence(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public List<Restaurant> getRestaurantRecommendations(City city){
        return (List<Restaurant>) entityManager.createQuery(ORDERED_LEISURES_BY_CITI_AND_DATE)
                .setParameter("city",city)
                .setMaxResults(5)
                .getResultList();
    }


}
