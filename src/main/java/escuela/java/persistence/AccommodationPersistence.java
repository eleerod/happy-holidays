package escuela.java.persistence;

import escuela.java.model.Accommodation;
import escuela.java.model.City;
import javax.persistence.EntityManager;
import java.util.Date;
import java.util.List;

public class AccommodationPersistence {

    EntityManager entityManager;

    private static final String ORDERED_ACCOMODATIONS_BY_CITI_AND_DATE =
                    "SELECT a from Accommodation a " +
                    "WHERE a.city LIKE :city " +
                    "AND :startdate Between a.start_date AND a.end_date " +
                    "AND :enddate Between a.start_date AND a.end_date " +
                    "AND a.enabled = 1 " +
                    "ORDER BY a.sponsored DESC, a.rating DESC";

    public AccommodationPersistence(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public List<Accommodation> getAccomodationsRecomendations(Date initDate, Date endDate, City city){
        return (List<Accommodation>) entityManager.createQuery(ORDERED_ACCOMODATIONS_BY_CITI_AND_DATE)
                .setParameter("city",city)
                .setParameter("startdate", initDate)
                .setParameter("enddate", endDate)
                .setMaxResults(5)
                .getResultList();
    }

    public Accommodation getAccomodation(int id){
        return entityManager.find(Accommodation.class, id);
    }

    public void updateAccommodation(int id, Accommodation newAccommodation) {

        Accommodation actualAccomodation = entityManager.find(Accommodation.class, id);
        actualAccomodation.setName(newAccommodation.getName());
        actualAccomodation.setAddress(newAccommodation.getAddress());
        actualAccomodation.setEnabled(newAccommodation.isEnabled());
        actualAccomodation.setEnd_date(newAccommodation.getEnd_date());
        actualAccomodation.setStart_date(newAccommodation.getStart_date());
        actualAccomodation.setRating(newAccommodation.getRating());
        actualAccomodation.setSponsored(newAccommodation.isSponsored());
        actualAccomodation.setCity(newAccommodation.getCity());

        entityManager.getTransaction().begin();
        entityManager.persist(actualAccomodation);
        entityManager.getTransaction().commit();
    }

    public void createAccommodation(Accommodation newAccommodation) {
        entityManager.getTransaction().begin();
        entityManager.persist(newAccommodation);
        entityManager.getTransaction().commit();
    }

    public void modifyAccomodationPersistence(int id, boolean value){
        Accommodation accommodation = entityManager.find(Accommodation.class, id);
        accommodation.setEnabled(value);
        entityManager.getTransaction().begin();
        entityManager.persist(accommodation);
        entityManager.getTransaction().commit();

    }


}
