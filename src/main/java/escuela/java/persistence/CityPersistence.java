package escuela.java.persistence;


import escuela.java.model.City;
import javax.persistence.EntityManager;;
import java.util.List;

public class CityPersistence {

    private static final String GET_CITY_BY_NAME_AND_COUNTRY = "Select c from City c where c.name LIKE :name";
    private static final String GET_NUMBER_CITY_BY_NAME = "Select count (c.id) from City c where c.name LIKE :name";
    private static final String GET_ALL_CITIES = "Select c from City c";

    EntityManager entityManager;

    public CityPersistence(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public City getCity(String name) {
        return (City) entityManager.createQuery(GET_CITY_BY_NAME_AND_COUNTRY)
                .setParameter("name", name).getSingleResult();
    }

    public boolean existsCity(String name){
        return ((Long) entityManager.createQuery(GET_NUMBER_CITY_BY_NAME)
                .setParameter("name", name).getSingleResult()) > 0;
    }

    public List<City> getAllCities() {
        return (List<City>) entityManager.createQuery(GET_ALL_CITIES).getResultList();
    }


    public City getCity(int id) {
        return entityManager.find(City.class, id);
    }

    public void updateCity(int id, City newCity) {
        City actualCity = entityManager.find(City.class, id);
        actualCity.setName(newCity.getName());
        actualCity.setCountry(newCity.getCountry());
        entityManager.getTransaction().begin();
        entityManager.persist(actualCity);
        entityManager.getTransaction().commit();
    }

    public void addCity(City newCity){
        entityManager.getTransaction().begin();
        entityManager.persist(newCity);
        entityManager.getTransaction().commit();
    }
}