package escuela.java.webservices;

import escuela.java.Utilities.EntityManagerUtilities;
import escuela.java.model.Accommodation;
import escuela.java.persistence.AccommodationPersistence;
import javax.persistence.EntityManager;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Path("accommodations")
public class AccommodationsResource {

    EntityManager entityManager = EntityManagerUtilities.getEntityManager();
    AccommodationPersistence accommodationPersistence = new AccommodationPersistence(entityManager);

    @GET
    @Path("{id}")
    public Accommodation getAccomodation(@PathParam("id") int id) {
        return accommodationPersistence.getAccomodation(id);
    }

    @PUT
    @Path("{id}")
    public Response modifyAccommodation(Accommodation accommodation, @PathParam("id") int id){
        accommodationPersistence.updateAccommodation(id, accommodation);
        return Response.ok().build();
    }

    @POST
    public Response createAccommodations(Accommodation accommodation){
        accommodationPersistence.createAccommodation(accommodation);
        return Response.ok().build();
    }

    @PATCH
    @Path("{id}/disable")
    public Response desactivateAccomodation(@PathParam("id") int id){
        accommodationPersistence.modifyAccomodationPersistence(id, false);
        return Response.ok().build();
    }
}
