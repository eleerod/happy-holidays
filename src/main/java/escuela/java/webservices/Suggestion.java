package escuela.java.webservices;

import escuela.java.model.Accommodation;
import escuela.java.model.Leisure;
import escuela.java.model.Restaurant;
import java.util.List;

public class Suggestion {
    private List<Leisure> leisures;
    private List<Restaurant> restaurants;
    private List<Accommodation> accommodations;

    public Suggestion() {
    }

    public List<Leisure> getLeisures() {
        return leisures;
    }

    public void setLeisures(List<Leisure> leisures) {
        this.leisures = leisures;
    }

    public List<Restaurant> getRestaurants() {
        return restaurants;
    }

    public void setRestaurants(List<Restaurant> restaurants) {
        this.restaurants = restaurants;
    }

    public List<Accommodation> getAccommodations() {
        return accommodations;
    }

    public void setAccommodations(List<Accommodation> accommodations) {
        this.accommodations = accommodations;
    }

    //@JsonbTransient
    public boolean ckeckIfisEmpty(){
        if((leisures == null || leisures.isEmpty()) && (restaurants == null || restaurants.isEmpty())
            && (accommodations == null || accommodations.isEmpty())) {
            return true;
        }
        return false;
    }
}
