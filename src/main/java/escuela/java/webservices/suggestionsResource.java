package escuela.java.webservices;

import escuela.java.Utilities.DateUtilities;
import escuela.java.Utilities.EntityManagerUtilities;
import escuela.java.model.Accommodation;
import escuela.java.model.City;
import escuela.java.model.Leisure;
import escuela.java.model.Restaurant;
import escuela.java.persistence.AccommodationPersistence;
import escuela.java.persistence.CityPersistence;
import escuela.java.persistence.LeisurePersistence;
import escuela.java.persistence.RestaurantPersistence;

import javax.persistence.EntityManager;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

@Path("suggestions")
public class suggestionsResource {

    private EntityManager entityManager = EntityManagerUtilities.getEntityManager();
    private CityPersistence cityPersistence = new CityPersistence(entityManager);

    @GET
    @Path("{product}/{destination}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getSuggestions(@PathParam("product") String product,
                          @PathParam("destination") String destination,
                          @QueryParam("start-date") String startDate,
                          @QueryParam("end-date") String endDate) {

        Date start_date = null;
        Date end_date = null;
        try {
            start_date = DateUtilities.getDateFromString(startDate);
            end_date = DateUtilities.getDateFromString(endDate);
        }catch(IllegalArgumentException e){
            return Response.status(400, e.getMessage()).build();
        }catch(ParseException pe){
            return Response.status(400, "date format is not correct").build();
            //Reviseeeeeeee
        }
        if(!SuggestionsResourceValidator.checkProduct(product)){
            return Response.status(400, "product is not valid").build();
        }
        if(!DateUtilities.checkDateAfterToday(end_date)){
            return Response.status(400, "date before current date").build();
        }
        if(!DateUtilities.checkDateAfterToday(start_date)){
            return Response.status(400, "date before current date").build();
        }
        if(!DateUtilities.checkDatesOrder(start_date, end_date)){
            return Response.status(400, "dates order is not correct").build();
        }
        if(!cityPersistence.existsCity(destination)){
            return Response.status(400, "city does not exist").build();
        }
        City city = cityPersistence.getCity(destination);

        Suggestion suggestion = new Suggestion();
        if(product.equals("flight")) {
            suggestion.setAccommodations(getRecommendedAccomodations(start_date, end_date, city));
        }
        suggestion.setLeisures(getRecommendedLeisures(start_date, end_date,city));
        suggestion.setRestaurants(getRecommendedRestaurants(city));
        checkIfEmptySuggestions(suggestion);
        return Response.ok(suggestion).build();
    }

    private List<Leisure> getRecommendedLeisures(Date startDate, Date endDate, City city){
        LeisurePersistence leisurePersistence = new LeisurePersistence(entityManager);
        return leisurePersistence.getLeisureRecommendations(startDate, endDate, city);
    }

    private List<Restaurant> getRecommendedRestaurants(City city){
        RestaurantPersistence restaurantPersistence = new RestaurantPersistence(entityManager);
        return restaurantPersistence.getRestaurantRecommendations(city);

    }
    private List<Accommodation> getRecommendedAccomodations(Date startDate, Date endDate, City city){
        AccommodationPersistence accommodationPersistence = new AccommodationPersistence(entityManager);
        return accommodationPersistence.getAccomodationsRecomendations(startDate, endDate, city);
    }

    private void checkIfEmptySuggestions(Suggestion suggestion){
        if(suggestion.ckeckIfisEmpty()){
            throw new WebApplicationException(Response.status(404, "No suggestions").build());
        }
    }


}
