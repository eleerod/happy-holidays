package escuela.java.webservices;

import escuela.java.Utilities.EntityManagerUtilities;
import escuela.java.model.City;
import escuela.java.persistence.CityPersistence;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("cities")
public class CitiesResources {
    CityPersistence cityPersistence = new CityPersistence(EntityManagerUtilities.getEntityManager());

    @GET
    public List<City> getAllCities(){
        return cityPersistence.getAllCities();
    }

    @GET
    @Path("{id}")
    public City getCity(@PathParam("id") int id){
        return cityPersistence.getCity(id);
    }

    @PUT
    @Path("{id}")
    public Response modifyCity(@PathParam("id") int id, City city){
        if(cityPersistence.getCity(id) == null){
            throw new WebApplicationException(Response.status(400, "City does not exist").build());
        }
        cityPersistence.updateCity(id,city);
        return Response.ok().build();

    }
    @POST
    public Response addCity(City city){
        cityPersistence.addCity(city);
        return Response.ok().build();

    }
}
