package escuela.java.Utilities;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class EntityManagerUtilities {

        private static final EntityManagerFactory emFactory;

        static {
            emFactory = Persistence.createEntityManagerFactory("JPADemo");
        }

        public static EntityManager getEntityManager() {
            return emFactory.createEntityManager();
        }

        public static void close() {
            emFactory.close();
        }
}
