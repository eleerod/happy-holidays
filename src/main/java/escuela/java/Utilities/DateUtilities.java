package escuela.java.Utilities;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class DateUtilities {

    private final static String DATE_FORMAT = "yyyy-MM-dd";
    private final static DateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);

    public static boolean checkDatesOrder(Date startDate, Date endDate) {
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        if(startDate.after(endDate)){
            return false;
        }
        return true;
    }

    public static boolean checkDateAfterToday(Date date){
        Date today = new Date();
        if(!date.after(today)){
            return false;
        }
        return true;
    }

    public static Date getDateFromString(String date) throws ParseException {
        if(date == null) {
            throw new IllegalArgumentException("date parameter is required");
        }
        dateFormat.setLenient(false);
        Date datea = dateFormat.parse(date);
        return datea;
    }

    public static String getFomatedDate(Date date){
            return dateFormat.format(date);
    }

}
