package escuela.java.model;

import javax.json.bind.annotation.JsonbPropertyOrder;
import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@JsonbPropertyOrder({"id","name","country"})
public class City {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String name;
    private String country;

    @OneToMany(mappedBy = "city")
    List<Leisure> leisures;

    @OneToMany(mappedBy = "city")
    List<Accommodation> accommodations;

    @OneToMany(mappedBy = "city")
    List<Restaurant> restaurants;

    public City(){

    }

    public City(int id, String name, String country) {
        this.id = id;
        this.name = name;
        this.country = country;
    }

    public City(String name, String country) {
        this.name = name;
        this.country = country;
    }

    public int getId() {
        return id;
    }

    public void setId(int city_id) {
        this.id = city_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @JsonbTransient
    public void setAccommodations(List<Accommodation> accommodations) {
        this.accommodations = accommodations;
    }

    @JsonbTransient
    public List<Restaurant> getRestaurants() {
        return restaurants;
    }

    @JsonbTransient
    public void setRestaurants(List<Restaurant> restaurants) {
        this.restaurants = restaurants;
    }

    @JsonbTransient
    public List<Leisure> getLeisures() {
        return leisures;
    }

    @JsonbTransient
    public void setLeisures(List<Leisure> leisures) {
        this.leisures = leisures;
    }

    @JsonbTransient
    public List<Accommodation> getAccommodations() {
        return accommodations;
    }



    @Override
    public String toString() {
        return "City{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", country='" + country + '\'' +
                '}';
    }
}
