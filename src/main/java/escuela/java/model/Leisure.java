package escuela.java.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.json.bind.annotation.JsonbDateFormat;
import javax.json.bind.annotation.JsonbPropertyOrder;
import javax.persistence.*;
import java.util.Date;

@Entity
@JsonbPropertyOrder({"id","name","address","rating","start_date","end_date","sponsored","enabled","city"})
public class Leisure {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String name;
    private String address;
    private Double rating;

    @JsonbDateFormat("yyyy.MM.dd")
    private Date start_date;

    @JsonbDateFormat("yyyy.MM.dd")
    private Date end_date;
    private boolean sponsored;
    private boolean enabled;

    @ManyToOne
    private City city;

    public Leisure(String name, String address, Double rating) {
        this.name = name;
        this.address = address;
        this.rating = rating;
    }

    public Leisure() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public Date getStart_date() {
        return start_date;
    }

    public void setStart_date(Date start_date) {
        this.start_date = start_date;
    }

    public Date getEnd_date() {
        return end_date;
    }

    public void setEnd_date(Date end_date) {
        this.end_date = end_date;
    }

    public boolean isSponsored() {
        return sponsored;
    }

    public void setSponsored(boolean sponsored) {
        this.sponsored = sponsored;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return "Leisure{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", rating=" + rating +
                ", start_date=" + start_date +
                ", end_date=" + end_date +
                ", sponsored=" + sponsored +
                ", enabled=" + enabled +
                ", city=" + city.toString() +
                '}';
    }
}
