package escuela.java.model;

import javax.json.bind.annotation.JsonbPropertyOrder;
import javax.persistence.*;

@Entity
@JsonbPropertyOrder({"id","name","address","rating","sponsored","enabled","city"})
public class Restaurant {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String name;
    private String address;
    private double rating;
    private boolean sponsored;
    private boolean enabled;

    @ManyToOne
    private City city;

    public Restaurant(){

    }

    public Restaurant(String name, String address, Double rating) {
        this.name = name;
        this.address = address;
        this.rating = rating;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public boolean isSponsored() {
        return sponsored;
    }

    public void setSponsored(boolean sponsored) {
        this.sponsored = sponsored;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return "Restaurant{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", rating=" + rating +
                ", sponsored=" + sponsored +
                ", enabled=" + enabled +
                ", city=" + city.toString() +
                '}';
    }
}
