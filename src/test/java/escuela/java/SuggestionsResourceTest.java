package escuela.java;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import escuela.java.Utilities.DateUtilities;
import org.glassfish.grizzly.http.server.HttpServer;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

public class SuggestionsResourceTest {

    private HttpServer server;
    private WebTarget target;

    @Before
    public void setUp() throws Exception {
        // start the server
        server = Main.startServer();
        // create the client
        Client c = ClientBuilder.newClient();

        // uncomment the following line if you want to enable
        // support for JSON in the client (you also have to uncomment
        // dependency on jersey-media-json module in pom.xml and Main.startServer())
        // --
       // c.configuration().enable(new org.glassfish.jersey.media.json.JsonJaxbFeature());

        target = c.target(Main.BASE_URI);
    }

    @After
    public void tearDown() throws Exception {
        server.stop();
    }

    @Test
    public void testGetSuggestions() throws ParseException {
        Response response = target.path("suggestions/flight/barcelona")
                .queryParam("start-date", "9999-07-21")
                .queryParam("end-date", "9999-07-21")
                .request().get(Response.class);

        int status = response.getStatus();
        Assert.assertEquals(200,  status);
    }

    @Test
    public void testGetSuggestionsWithBadFormatThenStatus400() throws ParseException {
        Response response = target.path("suggestions/flight/barcelona")
                .queryParam("start-date", "9999-07-54")
                .queryParam("end-date", "9999-07-21")
                .request().get(Response.class);

        int status = response.getStatus();
        String responseMessage = response.getStatusInfo().toString();
        Assert.assertEquals(400,  status);
        Assert.assertEquals("date format is not correct",  responseMessage);
    }

    @Test
    public void testGetSuggestionsWithoutDateThenStatus400() throws ParseException {
        Response response = target.path("suggestions/flight/barcelona")
                .queryParam("end-date", "9999-07-21")
                .request().get(Response.class);

        int status = response.getStatus();
        String responseMessage = response.getStatusInfo().toString();
        Assert.assertEquals(400,  status);
        Assert.assertEquals("date parameter is required",  responseMessage);
    }

    @Test
    public void testGetSuggestionsWithNotValidProductThenStatus400() throws ParseException {
        Response response = target.path("suggestions/badProduct/barcelona")
                .queryParam("start-date", "9999-07-03")
                .queryParam("end-date", "9999-07-21")
                .request().get(Response.class);

        int status = response.getStatus();
        String responseMessage = response.getStatusInfo().toString();
        Assert.assertEquals(400,  status);
        Assert.assertEquals("product is not valid",  responseMessage);
    }

    @Test
    public void testGetSuggestionsWithDateBeforeCurrentDateThenStatus400() throws ParseException {
        Calendar dayAfter = Calendar.getInstance();
        dayAfter.set(Calendar.DAY_OF_MONTH, (dayAfter.get(Calendar.DAY_OF_MONTH) - 1));
        Date date = dayAfter.getTime();

        Response response = target.path("suggestions/flight/barcelona")
                .queryParam("start-date", DateUtilities.getFomatedDate(date))
                .queryParam("end-date", DateUtilities.getFomatedDate(date))
                .request().get(Response.class);

        int status = response.getStatus();
        String responseMessage = response.getStatusInfo().toString();
        Assert.assertEquals(400,  status);
        Assert.assertEquals("date before current date",  responseMessage);
    }

    @Test
    public void testGetSuggestionsWithDatesIncorrectOrderThenStatus400() throws ParseException {
        Response response = target.path("suggestions/flight/barcelona")
                .queryParam("start-date", "9999-07-03")
                .queryParam("end-date", "9998-07-21")
                .request().get(Response.class);

        int status = response.getStatus();
        String responseMessage = response.getStatusInfo().toString();
        Assert.assertEquals(400,  status);
        Assert.assertEquals("dates order is not correct",  responseMessage);
    }

    @Test
    public void testGetSuggestionsWithInexistentCityThenStatus400() throws ParseException {
        Response response = target.path("suggestions/flight/NoCity")
                .queryParam("start-date", "9999-07-20")
                .queryParam("end-date", "9999-07-21")
                .request().get(Response.class);

        int status = response.getStatus();
        String responseMessage = response.getStatusInfo().toString();
        Assert.assertEquals(400,  status);
        Assert.assertEquals("city does not exist",  responseMessage);
    }

    @Test
    public void testGetSuggestionsReturnZeroSuggestionsThenStatus404() throws ParseException {
        Response response = target.path("suggestions/flight/NewCity")
                .queryParam("start-date", "9999-07-20")
                .queryParam("end-date", "9999-07-21")
                .request().get(Response.class);

        int status = response.getStatus();
        String responseMessage = response.getStatusInfo().toString();
        Assert.assertEquals(404,  status);
        Assert.assertEquals("No suggestions",  responseMessage);
    }
}
