package escuela.java;

import escuela.java.model.Accommodation;
import escuela.java.model.Leisure;
import escuela.java.model.Restaurant;
import escuela.java.webservices.Suggestion;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class SuggestionTest {

    @Test
    public void TestNotSetSuggestionThenIsEmpty(){
        Suggestion suggestion = new Suggestion();
        boolean isEmpty = suggestion.ckeckIfisEmpty();
        Assert.assertEquals(true, isEmpty);
    }

    @Test
    public void TestSuggestionSetWithEmptyListThenIsEmpty(){
        Suggestion suggestion = new Suggestion();
        suggestion.setRestaurants(new ArrayList<Restaurant>());
        suggestion.setLeisures(new ArrayList<Leisure>());
        suggestion.setAccommodations(new ArrayList<Accommodation>());
        boolean isEmpty = suggestion.ckeckIfisEmpty();
        Assert.assertEquals(true, isEmpty);
    }

    @Test
    public void TestSuggestionsSetRestaurantsThenNoIsEmpty(){
        Suggestion suggestion = new Suggestion();
        List<Restaurant> restaurants = new ArrayList<>();
        restaurants.add(new Restaurant("name","address",9.5));
        suggestion.setRestaurants(restaurants);
        boolean isEmpty = suggestion.ckeckIfisEmpty();
        Assert.assertEquals(false, isEmpty);
    }
    @Test
    public void TestSuggestionsSetLeissuresThenNoIsEmpty(){
        Suggestion suggestion = new Suggestion();
        List<Leisure> leisures = new ArrayList<>();
        leisures.add(new Leisure("name","address",9.5));
        suggestion.setLeisures(leisures);
        boolean isEmpty = suggestion.ckeckIfisEmpty();
        Assert.assertEquals(false, isEmpty);
    }
    @Test
    public void TestSuggestionsSetAccommodationsThenNoIsEmpty(){
        Suggestion suggestion = new Suggestion();
        List<Accommodation> accommodations = new ArrayList<>();
        accommodations.add(new Accommodation("name","address",9.5));
        suggestion.setAccommodations(accommodations);
        boolean isEmpty = suggestion.ckeckIfisEmpty();
        Assert.assertEquals(false, isEmpty);
    }

}
