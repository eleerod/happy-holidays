package escuela.java;

import escuela.java.webservices.Suggestion;
import escuela.java.webservices.SuggestionsResourceValidator;
import org.junit.Assert;
import org.junit.Test;

public class SuggestionsResourceValidatorTest {
    @Test
    public void TestCheckProductNotValidThenReturnFalse(){
        boolean result = SuggestionsResourceValidator.checkProduct("notValidProduct");
        Assert.assertEquals(false, result);
    }

    @Test
    public void TestCheckFlightThenReturnTrue(){
        boolean result = SuggestionsResourceValidator.checkProduct("flight");
        Assert.assertEquals(true, result);
    }

    @Test
    public void TestCheckAccommodationThenReturnTrue(){
        boolean result = SuggestionsResourceValidator.checkProduct("accommodation");
        Assert.assertEquals(true, result);
    }



}
