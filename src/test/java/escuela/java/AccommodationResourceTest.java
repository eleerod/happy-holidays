package escuela.java;

import javax.json.Json;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import escuela.java.webservices.Suggestion;
import org.glassfish.grizzly.http.server.HttpServer;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class AccommodationResourceTest {

    private HttpServer server;
    private WebTarget target;

    @Before
    public void setUp() throws Exception {
        // start the server
        server = Main.startServer();
        // create the client
        Client c = ClientBuilder.newClient();

        // uncomment the following line if you want to enable
        // support for JSON in the client (you also have to uncomment
        // dependency on jersey-media-json module in pom.xml and Main.startServer())
        // --
        //c.configuration().enable(new org.glassfish.jersey.media.json.JsonJaxbFeature());

        target = c.target(Main.BASE_URI);
    }

    @After
    public void tearDown() throws Exception {
        server.stop();
    }

    /**
     * Test to see that the message "Got it!" is sent in the response.
     */
    @Test
    public void addNewAccommodation() {
        JsonObjectBuilder jsonObjectBuilder = Json.createObjectBuilder();
        JsonObjectBuilder jsonCityBuilder = Json.createObjectBuilder();
        jsonCityBuilder.add("country","ES");
        jsonCityBuilder.add("id",1);
        jsonCityBuilder.add("name","Barcelona");
        jsonObjectBuilder.add("name","Hostal Henrique");
        jsonObjectBuilder.add("address","Calle falsa");
        jsonObjectBuilder.add("rating",3);
        jsonObjectBuilder.add("start_date","2018-08-01T00:00:00Z[UTC]");
        jsonObjectBuilder.add("end_date","2018-08-01T00:00:00Z[UTC]");
        jsonObjectBuilder.add("city",jsonCityBuilder);

         Response response = target.path("accommodations")
                .request(MediaType.APPLICATION_JSON).post(Entity.json(jsonObjectBuilder.build()));
        Assert.assertEquals("OK",  response.getStatusInfo().toString());
    }

    @Test
    public void getAccommodation() {
        Response response = target.path("accommodations/4")
                .request().get(Response.class);
        Assert.assertEquals("OK",  response.getStatusInfo().toString());
    }
}
