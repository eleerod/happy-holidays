package escuela.java;

import escuela.java.Utilities.DateUtilities;
import org.junit.Assert;
import org.junit.Test;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

public class DateUtilitesTest {
    @Test
    public void TestcheckDatesNotInOrderThenReturnFalse(){
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017,04,03);
        Date date = calendar.getTime();
        calendar.set(2017,04,02);
        Date date2 = calendar.getTime();
        Boolean datesInOrder = DateUtilities.checkDatesOrder(date, date2);
        Assert.assertEquals(false, datesInOrder);
    }
    @Test
    public void TestcheckDatesInOrderThenReturnTrue(){
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017,04,02);
        Date date = calendar.getTime();
        calendar.set(2017,04,03);
        Date date2 = calendar.getTime();
        Boolean datesInOrder = DateUtilities.checkDatesOrder(date, date2);
        Assert.assertEquals(true, datesInOrder);
    }

    @Test
    public void TestcheckDateAfterTodayThenReturnTrue(){
        Calendar dayAfter = Calendar.getInstance();
        dayAfter.set(Calendar.DAY_OF_MONTH, (dayAfter.get(Calendar.DAY_OF_MONTH) + 1));
        Date date = dayAfter.getTime();
        Assert.assertEquals(true, DateUtilities.checkDateAfterToday(date));
    }

    @Test
    public void TestCheckDateBeforeTodayThenReturnFalse(){
        Calendar dayBefore = Calendar.getInstance();
        dayBefore.set(Calendar.DAY_OF_MONTH, (dayBefore.get(Calendar.DAY_OF_MONTH) - 1));
        Date date = dayBefore.getTime();
        Assert.assertEquals(false, DateUtilities.checkDateAfterToday(date));
    }

    @Test (expected = IllegalArgumentException.class)
    public void TestGetDateFromStringWithNullDateThenThrowsException() throws ParseException {
        DateUtilities.getDateFromString(null);
    }

    @Test
    public void TestGetDateFromString() throws ParseException {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017,03,02);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        Date date = calendar.getTime();
        Assert.assertEquals(date.getTime(), DateUtilities.getDateFromString("2017-04-02").getTime());
    }

    @Test(expected = ParseException.class)
    public void TestGetDateFromStringWithBadFormatThenThrowParseException() throws ParseException{
        DateUtilities.getDateFromString("2028/09-03");
    }

    @Test(expected = ParseException.class)
    public void TestGetDateFromStringWithBadDayThenThrowParseException() throws ParseException{
        DateUtilities.getDateFromString("2018-12-32");
    }
    @Test(expected = ParseException.class)
    public void TestGetDateFromStringWithBadMonthThenThrowParseException() throws ParseException{
        DateUtilities.getDateFromString("2018-0-02");
    }

    @Test(expected = ParseException.class)
    public void TestGetDateFromStringWithBadYearThenThrowParseException() throws ParseException{
        DateUtilities.getDateFromString("0-04-01");
    }
}
